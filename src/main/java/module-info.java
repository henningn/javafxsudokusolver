module sudoku.javafxsudokusolver {
  requires javafx.controls;
  requires javafx.fxml;

  opens sudoku to javafx.fxml;
  exports sudoku;
  exports sudoku.controller;
  opens sudoku.controller to javafx.fxml;
}
