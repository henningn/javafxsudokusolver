/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.controller;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class CellController {
  private List<Label> possibleNumberLabels;
  private final Set<Integer> possibleNumbers = new TreeSet<>();

  @FXML
  private Label possibleNumber1;
  @FXML
  private Label possibleNumber2;
  @FXML
  private Label possibleNumber3;
  @FXML
  private Label possibleNumber4;
  @FXML
  private Label possibleNumber5;
  @FXML
  private Label possibleNumber6;
  @FXML
  private Label possibleNumber7;
  @FXML
  private Label possibleNumber8;
  @FXML
  private Label possibleNumber9;
  @FXML
  private Label finalNumberLabel;
  @FXML
  private TextField textfield;
  @FXML
  private Label redLayer;

  public void start() {
    showTextfield(false);
    resetPossibleNumbers();
    if (!textfield.getText().isEmpty()) {
      int givenFinalNumber = Integer.parseInt(textfield.getText());
      for (int i = 1; i <= 9; i++) {
        if (i != givenFinalNumber) {
          removePossibleNumber(i, true);
        }
      }
    }
    showRedLayer(false);
  }

  public void stop() {
    Integer finalNumber = getFinalNumber();
    if (finalNumber != null) {
      Platform.runLater(() -> textfield.setText(String.valueOf(finalNumber)));
    }
    showTextfield(true);
    resetPossibleNumbers();
    showRedLayer(false);
  }

  public void clear() {
    Platform.runLater(() -> textfield.setText(""));
    showTextfield(true);
    resetPossibleNumbers();
    showRedLayer(false);
  }

  public void setTextfieldText(String string) {
    Platform.runLater(() -> textfield.setText(string));
  }

  private void showTextfield(boolean show) {
    if (show) {
      fade(textfield, 0, 1, 400);
    } else {
      fade(textfield, 1, 0, 400);
    }
  }

  public void showRedLayer(boolean show) {
    if (show) {
      fade(redLayer, 0, 0.3, 200);
    } else {
      fade(redLayer, 0.3, 0, 200);
    }
  }

  public Integer getFinalNumber() {
    return getPossibleNumbers().size() == 1 ? getPossibleNumbers().iterator().next() : null;
  }

  public void setFinalNumber(int number) {
    for (int i = 1; i <= 9; i++) {
      if (i != number) {
        removePossibleNumber(i);
      }
    }
  }

  public Set<Integer> getPossibleNumbers() {
    return possibleNumbers;
  }

  private List<Label> getPossibleNumberLabels() {
    if (possibleNumberLabels == null) {
      possibleNumberLabels = new ArrayList<>(9);
      possibleNumberLabels.add(possibleNumber1);
      possibleNumberLabels.add(possibleNumber2);
      possibleNumberLabels.add(possibleNumber3);
      possibleNumberLabels.add(possibleNumber4);
      possibleNumberLabels.add(possibleNumber5);
      possibleNumberLabels.add(possibleNumber6);
      possibleNumberLabels.add(possibleNumber7);
      possibleNumberLabels.add(possibleNumber8);
      possibleNumberLabels.add(possibleNumber9);
    }
    return possibleNumberLabels;
  }

  private void resetPossibleNumbers() {
    getPossibleNumbers().clear();
    for (int pn = 1; pn <= 9; pn++) {
      getPossibleNumbers().add(pn);
    }
    updateUI();
  }

  public void removePossibleNumber(int number) {
    removePossibleNumber(number, false);
  }

  private void removePossibleNumber(int number, boolean instant) {
    try {
      int sizeBefore = getPossibleNumbers().size();
      getPossibleNumbers().remove(number);
      if (sizeBefore > getPossibleNumbers().size()) {
        updateUI();
        Thread.sleep(instant ? 1 : 200);
      }
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    if (getPossibleNumbers().isEmpty()) {
      System.err.println("Error: zero possible numbers");
    }
  }

  private void updateUI() {
    getPossibleNumberLabels().forEach(possibleNumberLabel -> {
      Integer possibleNumber = Integer.valueOf(possibleNumberLabel.getText());

      Platform.runLater(() -> {
        FadeTransition fadeTransition = new FadeTransition(Duration.millis(400), possibleNumberLabel);
        boolean doAnimation;
        if (getPossibleNumbers().contains(possibleNumber) && !possibleNumberLabel.isVisible()) {
          fadeTransition.setFromValue(0);
          fadeTransition.setToValue(1);
          doAnimation = true;
        } else if (!getPossibleNumbers().contains(possibleNumber) && possibleNumberLabel.isVisible()) {
          fadeTransition.setFromValue(1);
          fadeTransition.setToValue(0);
          doAnimation = true;
        } else {
          doAnimation = false;
        }
        fadeTransition.setOnFinished(event -> {
          possibleNumberLabel.setVisible(getPossibleNumbers().contains(possibleNumber));
        });
        if (doAnimation) {
          fadeTransition.play();
        }
      });
    });

    Platform.runLater(() -> {
      finalNumberLabel.setText(getFinalNumber() != null ? getFinalNumber().toString() : "");

      FadeTransition fadeTransition = new FadeTransition(Duration.millis(400), finalNumberLabel);
      boolean doAnimation;
      if (getFinalNumber() != null && !finalNumberLabel.isVisible()) {
        fadeTransition.setFromValue(0);
        fadeTransition.setToValue(1);
        doAnimation = true;
      } else if (getFinalNumber() == null && finalNumberLabel.isVisible()) {
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);
        doAnimation = true;
      } else {
        doAnimation = false;
      }
      fadeTransition.setOnFinished(event -> {
        finalNumberLabel.setVisible(getFinalNumber() != null);
      });
      if (doAnimation) {
        fadeTransition.play();
      }
    });
  }

  private void fade(Node node, double from, double to, double duration) {
    Platform.runLater(() -> {
      node.setVisible(true);
      FadeTransition ft = new FadeTransition(Duration.millis(duration), node);
      ft.setFromValue(from);
      ft.setToValue(to);
      ft.play();
    });
  }
}
