/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import sudoku.PreSetBuilder;
import sudoku.SolverThread;
import sudoku.solver.Solver;

import java.util.LinkedList;
import java.util.List;

public class MainSceneController {

  @FXML
  private PlaygroundController playgroundController;

  @FXML
  private Button startButton, stopButton, resetButton;
  private boolean pauseActive = false;
  private PreSetBuilder preSetBuilder;
  private List<Solver> solverThreads = new LinkedList<>();
  private SolverThread solverThread;

  public void initialize() {
    preSetBuilder = new PreSetBuilder(playgroundController);
    stopButton.setDisable(true);
    preSetBuilder.loadCurrentPreSet();
  }

  public void startSolving(ActionEvent actionEvent) {
    playgroundController.start();
    solverThreads.clear();
    if (solverThread == null) {
      solverThread = new SolverThread(playgroundController);
      solverThread.start();
    }

    startButton.setDisable(true);
    stopButton.setDisable(false);
  }

  public void stopSolving(ActionEvent actionEvent) {
    if (solverThread != null) {
      solverThread.interrupt();
      solverThread = null;
    }
    playgroundController.stop();

    startButton.setDisable(false);
    stopButton.setDisable(true);
  }

  public void clear(ActionEvent actionEvent) {
    if (solverThread != null) {
      solverThread.interrupt();
      solverThread = null;
    }
    playgroundController.clear();
  }
}
