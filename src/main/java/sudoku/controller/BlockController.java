/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.controller;

import javafx.fxml.FXML;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by henning on 30.10.15.
 */
public class BlockController {
  private List<CellController> cells;

  @FXML
  public CellController topLeftCellController;
  @FXML
  public CellController topCellController;
  @FXML
  public CellController topRightCellController;
  @FXML
  public CellController leftCellController;
  @FXML
  public CellController centerCellController;
  @FXML
  public CellController rightCellController;
  @FXML
  public CellController bottomLeftCellController;
  @FXML
  public CellController bottomCellController;
  @FXML
  public CellController bottomRightCellController;

  public void start() {
    getCells().forEach(CellController::start);
  }

  public void stop() {
    getCells().forEach(CellController::stop);
  }

  public void clear() {
    getCells().forEach(CellController::clear);
  }

  List<CellController> getCells() {
    if (cells == null) {
      cells = new ArrayList<>(9);
      cells.add(topLeftCellController);
      cells.add(topCellController);
      cells.add(topRightCellController);
      cells.add(leftCellController);
      cells.add(centerCellController);
      cells.add(rightCellController);
      cells.add(bottomLeftCellController);
      cells.add(bottomCellController);
      cells.add(bottomRightCellController);
    }
    return cells;
  }
}
