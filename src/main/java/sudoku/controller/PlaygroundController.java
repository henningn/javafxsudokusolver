/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.controller;

import javafx.fxml.FXML;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class PlaygroundController {
  private List<BlockController> blocks;

  @FXML
  public BlockController topLeftBlockController;
  @FXML
  public BlockController topBlockController;
  @FXML
  public BlockController topRightBlockController;
  @FXML
  public BlockController leftBlockController;
  @FXML
  public BlockController centerBlockController;
  @FXML
  public BlockController rightBlockController;
  @FXML
  public BlockController bottomLeftBlockController;
  @FXML
  public BlockController bottomBlockController;
  @FXML
  public BlockController bottomRightBlockController;

  public void start() {
    getBlocks().forEach(BlockController::start);
  }

  public void stop() {
    getBlocks().forEach(BlockController::stop);
  }

  public void clear() {
    getBlocks().forEach(BlockController::clear);
  }

  public List<CellController> getBlock(int number) {
    return getBlocks().get(number).getCells();
  }

  public List<CellController> getColumn(int columnNumber) {
    List<CellController> columnCells = new ArrayList<>(9);
    for (int i = 0; i < 9; i++) {
      columnCells.add(getCell(columnNumber, i));
    }
    return columnCells;
  }

  public List<CellController> getRow(int rowNumber) {
    List<CellController> rowCells = new ArrayList<>(9);
    for (int i = 0; i < 9; i++) {
      rowCells.add(getCell(i, rowNumber));
    }
    return rowCells;
  }

  public CellController getCell(int x, int y) {
    int blockIndex = (x / 3) + (y / 3) * 3;
    int cellIndex = (x % 3) + (y % 3) * 3;
    return getBlocks().get(blockIndex).getCells().get(cellIndex);
  }

  public boolean isEveryNumberRevealed() {
    for (int x = 0; x < 9; x++) {
      for (int y = 0; y < 9; y++) {
        if (getCell(x, y).getFinalNumber() == null) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean isValid() {
    for (int i = 0; i < 9; i++) {
      List<Integer> blockFinalNumbers = getBlock(i).stream()
        .map(CellController::getFinalNumber)
        .filter(Objects::nonNull)
        .toList();
      if (blockFinalNumbers.size() > new HashSet<>(blockFinalNumbers).size()) {
        return false;
      }

      List<Integer> rowFinalNumbers = getRow(i).stream()
        .map(CellController::getFinalNumber)
        .filter(Objects::nonNull)
        .toList();
      if (rowFinalNumbers.size() > new HashSet<>(rowFinalNumbers).size()) {
        return false;
      }

      List<Integer> columnFinalNumbers = getBlock(i).stream()
        .map(CellController::getFinalNumber)
        .filter(Objects::nonNull)
        .toList();
      if (columnFinalNumbers.size() > new HashSet<>(columnFinalNumbers).size()) {
        return false;
      }
    }
    return true;
  }

  private List<BlockController> getBlocks() {
    if (blocks == null) {
      blocks = new ArrayList<>(9);
      blocks.add(topLeftBlockController);
      blocks.add(topBlockController);
      blocks.add(topRightBlockController);
      blocks.add(leftBlockController);
      blocks.add(centerBlockController);
      blocks.add(rightBlockController);
      blocks.add(bottomLeftBlockController);
      blocks.add(bottomBlockController);
      blocks.add(bottomRightBlockController);
    }
    return blocks;
  }
}
