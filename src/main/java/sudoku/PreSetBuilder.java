/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku;

import sudoku.controller.PlaygroundController;

public class PreSetBuilder {
  PlaygroundController playgroundController;

  public PreSetBuilder(PlaygroundController playgroundController) {
    this.playgroundController = playgroundController;
  }

  public void loadCurrentPreSet() {
    unsolved_BinaryFission();
  }

  /**
   * Binary Fission by shye
   * <a href="hhttps://www.youtube.com/watch?v=ynkkMxQPUpk">YouTube</a>
   */
  private void unsolved_BinaryFission() {
    parsePreSet("""
      ..5.2.6..
      .9...4.1.
      2..5....3
      ..6.3....
      ...8.1...
      ....9.4..
      3....2..7
      .1.9...5.
      ..4.6.8..
      """);
  }

  /**
   * Tatooine Sunset by Philip Newman
   * <a href="https://www.youtube.com/watch?v=TQ0lso4fJzk">YouTube</a>
   */
  private void solved_TatooineSunset() {
    parsePreSet("""
      .........
      ..98....7
      .8..6..5.
      .5..4..3.
      ..79....2
      .........
      ..27....9
      .4..5..6.
      3....62..
      """);
  }

  /**
   * <a href="https://www.youtube.com/watch?v=gVT786t1Kjk">YouTube</a>
   */
  private void solvable_preSet9() {
    parsePreSet("""
      17.2..3..
      32.4.15..
      .56.37.12
      .37.12..5
      2.1.....3
      5..3..12.
      8.31..2..
      ..2..3..1
      .15.28.36
      """);
  }

  /**
   * <a href="https://www.youtube.com/watch?v=NZLsGrPqpy8">YouTube</a>
   */
  private void unsolved_preSet8() {
    parsePreSet("""
      .26547.1.
      .143.6.75
      5371.846.
      472839651
      365271..4
      198465..7
      2516847..
      783952146
      6497135..
      """);
  }

  /**
   * <a href="https://www.youtube.com/watch?v=DEfff5yTj-k">YouTube</a>
   */
  private void solvable_preSet7() {
    parsePreSet("""
      .34...67.
      2.......8
      1...4...9
      ...8.3...
      ..7...5..
      ...2.6...
      3...1...5
      7.......6
      .69...14.
      """);
  }

  private void unsolved_preSet6() {
    parsePreSet("""
      ...1.2...
      .6.....7.
      ..8...9..
      4.......3
      .5...7...
      2...8...1
      ..9...8.5
      .7.....6.
      ...3.4...
      """);
  }

  private void solvabled_preSet5() {
    parsePreSet("""
      1...8...6
      .2.4...7.
      ..3...5..
      ..4..7.1.
      5...3...8
      .6.2..9..
      ..7...2..
      .8...9.3.
      9...6...4
      """);
  }

  private void unsolved_preSet4() {
    parsePreSet("""
      8........
      ..36.....
      .7..9.2..
      .5...7...
      ....457..
      ...1...3.
      ..1....68
      ..85...1.
      .9....4..
      """);
  }

  private void solvable_preSet3() {
    parsePreSet("""
      ..6.27..9
      .8....54.
      ...4.5.6.
      .71..9.2.
      ..864....
      ..9.8....
      ...2....3
      12......5
      3..51...7
      """);
  }

  private void unsolved_preSet2() {
    parsePreSet("""
      .........
      ...5.1.9.
      .23..7...
      .....5..8
      ..6...3.2
      1....9...
      9......4.
      .........
      ....3.6..
      """);
  }

  private void solvable_preSet1() {
    parsePreSet("""
      53..7....
      6..195...
      .98....6.
      8...6...3
      4..8.3..1
      7...2...6
      .6....28.
      ...419..5
      ....8..79
      """);
  }

  private void parsePreSet(String preSet) {
    String[] preSetArray = preSet.split("\n");

    for (int x = 0; x < 9; x++) {
      for (int y = 0; y < 9; y++) {
        String value = String.valueOf(preSetArray[y].charAt(x));
        if (!value.equals(".")) {
          playgroundController.getCell(x, y).setTextfieldText(value);
        }
      }
    }
  }
}
