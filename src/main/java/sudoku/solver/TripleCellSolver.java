/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.ArrayList;
import java.util.List;

/**
 * TripleCellSolver is like the {@link TwinCellSolver} but with three cells.
 * Identify three possible numbers, that can be only be set in three cells. Remove other possible numbers in those
 * cells.
 */
public class TripleCellSolver implements Solver {
  private final List<CellController> cells;

  public TripleCellSolver(List<CellController> cells) {
    this.cells = cells;
  }

  @Override
  public void solve() {
    for (int firstCellNumber = 0; firstCellNumber < 7; firstCellNumber++) {
      CellController first = cells.get(firstCellNumber);
      if (first.getPossibleNumbers().size() > 1) {
        first.showRedLayer(true);

        for (int secondCellNumber = firstCellNumber + 1; secondCellNumber < 8; secondCellNumber++) {
          CellController second = cells.get(secondCellNumber);
          if (second.getPossibleNumbers().size() > 1) {
            second.showRedLayer(true);

            for (int thirdCellNumber = secondCellNumber + 1; thirdCellNumber < 9; thirdCellNumber++) {
              CellController third = cells.get(thirdCellNumber);
              if (third.getPossibleNumbers().size() > 1) {
                third.showRedLayer(true);

                List<CellController> selectedCells = new ArrayList<>(3);
                selectedCells.add(first);
                selectedCells.add(second);
                selectedCells.add(third);

                solve(selectedCells);

                third.showRedLayer(false);
              }
            }
            second.showRedLayer(false);
          }
        }
        try {
          Thread.sleep(30);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        first.showRedLayer(false);
      }
    }
  }

  private void solve(List<CellController> selectedCells) {
    List<CellController> remainingCells = cells.stream().filter(c -> !selectedCells.contains(c)).toList();

    for (int possibleNumberOne = 1; possibleNumberOne <= 7; possibleNumberOne++) {
      for (int possibleNumberTwo = possibleNumberOne + 1; possibleNumberTwo <= 8; possibleNumberTwo++) {
        for (int possibleNumberThree = possibleNumberTwo + 1; possibleNumberThree <= 9; possibleNumberThree++) {

          // 1 - 2 - 3 no -> SinglePossibleNumberSolver
          // 12 - 12 - 123 no -> SinglePossibleNumberSolver
          // 2 - 1 - 123 no -> SinglePossibleNumberSolver
          // 2 - 23 - 123 no -> SinglePossibleNumberSolver
          // 1 - 12 - 123 no -> SinglePossibleNumberSolver
          // 12 - 23 - 13 fine
          // 123 - 123 - 123 fine

          int[] possibleNumbers = {possibleNumberOne, possibleNumberTwo, possibleNumberThree};
          int[] possibleNumberCount = {0, 0, 0};
          for (int i = 0; i < possibleNumbers.length; i++) {
            for (CellController cell : selectedCells) {
              if (cell.getPossibleNumbers().contains(possibleNumbers[i])) {
                possibleNumberCount[i]++;
              }
            }
          }

          if (possibleNumberCount[0] >= 2 && possibleNumberCount[1] >= 2 && possibleNumberCount[2] >= 2
            && !contains(remainingCells, possibleNumberOne)
            && !contains(remainingCells, possibleNumberTwo)
            && !contains(remainingCells, possibleNumberThree)) {
            for (CellController selectedCell : selectedCells) {
              removeAllPossibleNumbersButGiven(selectedCell, possibleNumberOne, possibleNumberTwo, possibleNumberThree);
            }
          }
        }
      }
    }
  }

  private boolean contains(List<CellController> cells, int possibleNumber) {
    for (CellController cell : cells) {
      if (cell.getPossibleNumbers().contains(possibleNumber)) {
        return true;
      }
    }
    return false;
  }

  private void removeAllPossibleNumbersButGiven(CellController cell, int number1, int number2, int number3) {
    for (int i = 1; i <= 9; i++) {
      if (i != number1 && i != number2 && i != number3) {
        cell.removePossibleNumber(i);
      }
    }
  }
}
