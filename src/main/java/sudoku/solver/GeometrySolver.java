/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.ArrayList;
import java.util.List;

/**
 * Set1 and Set2 must have the same set of digits.
 */
public class GeometrySolver implements Solver {
  private final List<CellController> set1;
  private final List<CellController> set2;

  public GeometrySolver(List<CellController> set1, List<CellController> set2) {
    this.set1 = set1;
    this.set2 = set2;
  }

  @Override
  public void solve() {
    set1.forEach(c -> c.showRedLayer(true));
    set2.forEach(c -> c.showRedLayer(true));

    check(set1, set2);
    check(set2, set1);

    try {
      Thread.sleep(200);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    set1.forEach(c -> c.showRedLayer(false));
    set2.forEach(c -> c.showRedLayer(false));
  }

  private void check(List<CellController> checkCells, List<CellController> refCells) {
    List<CellController> cleanCheckCells = new ArrayList<>(checkCells);
    List<CellController> cleanRefCells = new ArrayList<>(refCells);

    for (int i = 1; i <= 9; i++) {
      CellController checkCell = null;
      CellController refCell = null;

      for (CellController cleanCheckCell : cleanCheckCells) {
        if (cleanCheckCell.getFinalNumber() != null && cleanCheckCell.getFinalNumber() == i) {
          checkCell = cleanCheckCell;

          for (CellController cleanRefCell : cleanRefCells) {
            if (cleanRefCell.getFinalNumber() != null && cleanRefCell.getFinalNumber() == i) {
              refCell = cleanRefCell;
            }
          }
        }
      }

      if (checkCell != null && refCell != null) {
        cleanCheckCells.remove(checkCell);
        cleanRefCells.remove(refCell);
        i--;
      }
    }

    List<CellController> openCheckCells = cleanCheckCells.stream().filter(c -> c.getFinalNumber() == null).toList();
    List<CellController> finalRefCells = cleanRefCells.stream().filter(c -> c.getFinalNumber() != null).toList();

    if (finalRefCells.size() >= openCheckCells.size()) {
      List<Integer> refFinalNumbers = finalRefCells.stream().map(CellController::getFinalNumber).toList();
      List<Integer> invertedRefFinalNumbers = getInvertedFinalNumbers(refFinalNumbers);
      openCheckCells.forEach(cell -> invertedRefFinalNumbers.forEach(cell::removePossibleNumber));
    }

    List<Integer> refPossibleNumbers = new ArrayList<>();
    cleanRefCells.forEach(cell -> refPossibleNumbers.addAll(cell.getPossibleNumbers()));
    List<Integer> invertedRefPossibleNumbers = getInvertedFinalNumbers(refPossibleNumbers);
    openCheckCells.forEach(cell -> invertedRefPossibleNumbers.forEach(cell::removePossibleNumber));
  }

  private List<Integer> getInvertedFinalNumbers(List<Integer> finalNumbers) {
    List<Integer> invertedFinalNumbers = new ArrayList<>();
    for (int i = 1; i <= 9; i++) {
      if (!finalNumbers.contains(i)) {
        invertedFinalNumbers.add(i);
      }
    }
    return invertedFinalNumbers;
  }
}
