/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * If a possible number is present in only one cell, this must be a final number.
 */
public class SinglePossibleNumberSolver implements Solver {
  private final List<CellController> cells;

  public SinglePossibleNumberSolver(List<CellController> cells) {
    this.cells = cells;
  }

  @Override
  public void solve() {
    cells.forEach(cell -> {
      if (cell.getFinalNumber() == null) {
        cell.showRedLayer(true);

        AtomicReference<Integer> finalNumber = new AtomicReference<>();
        cell.getPossibleNumbers().forEach(possibleNumber -> {
          if (cells.stream().filter(c -> c.getPossibleNumbers().contains(possibleNumber)).count() == 1) {
            finalNumber.set(possibleNumber);
          }
        });

        if (finalNumber.get() != null) {
          cell.setFinalNumber(finalNumber.get());
        }

        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        cell.showRedLayer(false);
      }
    });
  }
}
