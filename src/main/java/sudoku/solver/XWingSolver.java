/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;
import sudoku.controller.PlaygroundController;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * If there are two parallel lines with a possible number occur only in two cells in the line and the cells of both
 * lines align on the other axis, the specific possible number can be removed from all other lines on this axis.
 * For example: If the only spot for the possible number '4' is in cell 2 and 6 of line 3 and 7, all other lines cannot
 * have a 4 in cell 2 and 6.
 */
public class XWingSolver implements Solver {
  private final PlaygroundController playgroundController;

  public XWingSolver(PlaygroundController playgroundController) {
    this.playgroundController = playgroundController;
  }

  @Override
  public void solve() {
    solve(Direction.horizontal);
    solve(Direction.vertical);
  }

  private void solve(Direction direction) {
    for (int line1Index = 0; line1Index < 9; line1Index++) {
      List<CellController> line1 = getLine(direction, line1Index);
      line1.forEach(c -> c.showRedLayer(true));

      for (int line2Index = line1Index + 1; line2Index < 9; line2Index++) {
        List<CellController> line2 = getLine(direction, line2Index);
        line2.forEach(c -> c.showRedLayer(true));

        for (int possibleNumber = 1; possibleNumber <= 9; possibleNumber++) {
          List<Integer> line1PossibleNumberIndexes = getPossibleNumberIndexes(line1, possibleNumber);
          List<Integer> line2PossibleNumberIndexes = getPossibleNumberIndexes(line2, possibleNumber);

          if (line1PossibleNumberIndexes.size() == 2 && line2PossibleNumberIndexes.size() == 2
            && Objects.equals(line1PossibleNumberIndexes.get(0), line2PossibleNumberIndexes.get(0))
            && Objects.equals(line1PossibleNumberIndexes.get(1), line2PossibleNumberIndexes.get(1))) {

            List<CellController> counterLine1 = getCounterLine(direction, line1PossibleNumberIndexes.getFirst());
            List<CellController> counterLine2 = getCounterLine(direction, line1PossibleNumberIndexes.getLast());
            counterLine1.forEach(c -> c.showRedLayer(true));
            counterLine2.forEach(c -> c.showRedLayer(true));

            removePossibleNumber(line1Index, line2Index, counterLine1, possibleNumber);
            removePossibleNumber(line1Index, line2Index, counterLine2, possibleNumber);

            counterLine1.forEach(c -> c.showRedLayer(false));
            counterLine2.forEach(c -> c.showRedLayer(false));
          }
        }

        try {
          Thread.sleep(80);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        line2.forEach(c -> c.showRedLayer(false));
      }
      line1.forEach(c -> c.showRedLayer(false));
    }
  }

  private List<CellController> getLine(Direction direction, int index) {
    return Direction.horizontal.equals(direction)
      ? playgroundController.getRow(index)
      : playgroundController.getColumn(index);
  }

  private List<CellController> getCounterLine(Direction direction, int index) {
    return Direction.vertical.equals(direction)
      ? playgroundController.getRow(index)
      : playgroundController.getColumn(index);
  }

  private List<Integer> getPossibleNumberIndexes(List<CellController> cells, int possibleNumber) {
    List<Integer> possibleNumberIndexes = new ArrayList<>();

    for (int i = 0; i < 9; i++) {
      if (cells.get(i).getPossibleNumbers().contains(possibleNumber)) {
        possibleNumberIndexes.add(i);
      }
    }

    return possibleNumberIndexes;
  }

  private void removePossibleNumber(int line1Index, int line2Index,
    List<CellController> counterLine, int possibleNumber) {
    for (int i = 0; i < 9; i++) {
      if (i != line1Index && i != line2Index) {
        counterLine.get(i).removePossibleNumber(possibleNumber);
      }
    }
  }

  private enum Direction {
    horizontal, vertical
  }
}
