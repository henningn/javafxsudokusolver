/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.List;

/**
 * Simplest solver. If a cell has a final number, deactivate those possible number for all other cells.
 */
public class SimpleSolver implements Solver {
  private final List<CellController> cells;

  public SimpleSolver(List<CellController> cells) {
    this.cells = cells;
  }

  @Override
  public void solve() {
    cells.forEach(cellController -> {
      Integer finalNumber = cellController.getFinalNumber();
      if (finalNumber != null) {
        cellController.showRedLayer(true);
        cells.stream().filter(c -> c.getFinalNumber() == null).forEach(c -> c.removePossibleNumber(finalNumber));

        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
        cellController.showRedLayer(false);
      }
    });
  }
}
