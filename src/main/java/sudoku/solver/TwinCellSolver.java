/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.List;

/**
 * Identify two possible numbers, that can be only be set in two cells. Remove other possible numbers in those cells.
 */
public class TwinCellSolver implements Solver {
  private final List<CellController> cells;

  public TwinCellSolver(List<CellController> cells) {
    this.cells = cells;
  }

  @Override
  public void solve() {
    for (int firstCellNumber = 0; firstCellNumber < 9; firstCellNumber++) {
      CellController first = cells.get(firstCellNumber);
      if (first.getPossibleNumbers().size() > 1) {
        first.showRedLayer(true);

        for (int secondCellNumber = firstCellNumber + 1; secondCellNumber < 9; secondCellNumber++) {
          CellController second = cells.get(secondCellNumber);
          if (second.getPossibleNumbers().size() > 1) {
            second.showRedLayer(true);
            List<CellController> remainingCells = getRemainingCells(first, second);
            for (int possibleNumberOne = 1; possibleNumberOne <= 9; possibleNumberOne++) {
              for (int possibleNumberTwo = possibleNumberOne + 1; possibleNumberTwo <= 9; possibleNumberTwo++) {

                if (contains(first, possibleNumberOne, possibleNumberTwo)
                  && contains(second, possibleNumberOne, possibleNumberTwo)
                  && !contains(remainingCells, possibleNumberOne, possibleNumberTwo)) {

                  removeAllPossibleNumbersButGiven(first, possibleNumberOne, possibleNumberTwo);
                  removeAllPossibleNumbersButGiven(second, possibleNumberOne, possibleNumberTwo);
                }
              }
            }

            try {
              Thread.sleep(20);
            } catch (InterruptedException e) {
              throw new RuntimeException(e);
            }
            second.showRedLayer(false);
          }
        }
        first.showRedLayer(false);
      }
    }
  }

  private List<CellController> getRemainingCells(CellController first, CellController second) {
    return cells.stream().filter(c -> !c.equals(first) && !c.equals(second)).toList();
  }

  private boolean contains(List<CellController> cells, int possibleNumberOne, int possibleNumberTwo) {
    for (CellController cell : cells) {
      if (contains(cell, possibleNumberOne, possibleNumberTwo)) {
        return true;
      }
    }
    return false;
  }

  private boolean contains(CellController cell, int possibleNumberOne, int possibleNumberTwo) {
    return cell.getPossibleNumbers().contains(possibleNumberOne)
      || cell.getPossibleNumbers().contains(possibleNumberTwo);
  }

  private void removeAllPossibleNumbersButGiven(CellController cell, int number1, int number2) {
    for (int i = 1; i <= 9; i++) {
      if (i != number1 && i != number2) {
        cell.removePossibleNumber(i);
      }
    }
  }
}
