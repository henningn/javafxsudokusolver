/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;
import sudoku.controller.PlaygroundController;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Swordfish is like the {@link XWingSolver} but with three lines.
 * There have to be 3 parallel lines where a possible number can only in 2 or 3 cells. The cells of the line have to
 * align on the counter axis. If so, there have to be 3 counter lines. The possible number can be removed from the
 * remaining cells from the counter lines.
 * Example 1: The possible number '5' in row 3 can only be in cell 1 and cell 9. For row 4 it can be only in cell 4 and
 * cell 9. And for row 9 it can be only in cell 1 and cell 4.
 * The '5' cannot be in the remaining rows in cell 1, 4 or 9.
 * Example 2: The possible number '2' in row 2 can be in cell 1, cell 2 and cell 6. For row 5 it can be in cell 1,
 * cell 2 and cell 6. And for row 8 it can be in cell 1, cell 2 and cell 6.
 * The '2' cannot be in the remaining rows in cell 1, 2 or 6.
 */
public class SwordfishSolver implements Solver {
  private final PlaygroundController playgroundController;

  public SwordfishSolver(PlaygroundController playgroundController) {
    this.playgroundController = playgroundController;
  }

  @Override
  public void solve() {
    solve(Direction.horizontal);
    solve(Direction.vertical);
  }

  private void solve(Direction direction) {
    for (int line1Index = 0; line1Index < 7; line1Index++) {
      List<CellController> line1 = getLine(direction, line1Index);
      line1.forEach(c -> c.showRedLayer(true));

      for (int line2Index = line1Index + 1; line2Index < 8; line2Index++) {
        List<CellController> line2 = getLine(direction, line2Index);
        line2.forEach(c -> c.showRedLayer(true));

        for (int line3Index = line2Index + 1; line3Index < 9; line3Index++) {
          List<CellController> line3 = getLine(direction, line3Index);
          line3.forEach(c -> c.showRedLayer(true));

          for (int possibleNumber = 1; possibleNumber <= 9; possibleNumber++) {
            List<Integer> line1PossibleNumberIndexes = getPossibleNumberIndexes(line1, possibleNumber);
            List<Integer> line2PossibleNumberIndexes = getPossibleNumberIndexes(line2, possibleNumber);
            List<Integer> line3PossibleNumberIndexes = getPossibleNumberIndexes(line3, possibleNumber);

            if (line1PossibleNumberIndexes.size() >= 2 && line1PossibleNumberIndexes.size() <= 3
                && line2PossibleNumberIndexes.size() >= 2 && line2PossibleNumberIndexes.size() <= 3
                && line3PossibleNumberIndexes.size() >= 2 && line3PossibleNumberIndexes.size() <= 3) {

              Set<Integer> possibleNumberIndexes = new TreeSet<>();
              possibleNumberIndexes.addAll(line1PossibleNumberIndexes);
              possibleNumberIndexes.addAll(line2PossibleNumberIndexes);
              possibleNumberIndexes.addAll(line3PossibleNumberIndexes);

              if (possibleNumberIndexes.size() == 3) {
                for (Integer i : possibleNumberIndexes) {
                  List<CellController> counterLine = getCounterLine(direction, i);
                  counterLine.forEach(c -> c.showRedLayer(true));

                  removePossibleNumber(line1Index, line2Index, line3Index, counterLine, possibleNumber);

                  counterLine.forEach(c -> c.showRedLayer(false));
                }
              }
            }
          }

          try {
            Thread.sleep(80);
          } catch (InterruptedException e) {
            throw new RuntimeException(e);
          }
          line3.forEach(c -> c.showRedLayer(false));
        }
        line2.forEach(c -> c.showRedLayer(false));
      }
      line1.forEach(c -> c.showRedLayer(false));
    }
  }

  private List<CellController> getLine(Direction direction, int index) {
    return Direction.horizontal.equals(direction)
        ? playgroundController.getRow(index)
        : playgroundController.getColumn(index);
  }

  private List<CellController> getCounterLine(Direction direction, int index) {
    return Direction.vertical.equals(direction)
        ? playgroundController.getRow(index)
        : playgroundController.getColumn(index);
  }

  private List<Integer> getPossibleNumberIndexes(List<CellController> cells, int possibleNumber) {
    List<Integer> possibleNumberIndexes = new ArrayList<>();

    for (int i = 0; i < 9; i++) {
      if (cells.get(i).getPossibleNumbers().contains(possibleNumber)) {
        possibleNumberIndexes.add(i);
      }
    }

    return possibleNumberIndexes;
  }

  private void removePossibleNumber(int line1Index, int line2Index, int line3Index,
      List<CellController> counterLine, int possibleNumber) {
    for (int i = 0; i < 9; i++) {
      if (i != line1Index && i != line2Index && i != line3Index) {
        counterLine.get(i).removePossibleNumber(possibleNumber);
      }
    }
  }

  private enum Direction {
    horizontal, vertical
  }
}
