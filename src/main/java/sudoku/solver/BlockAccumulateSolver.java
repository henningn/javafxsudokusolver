/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku.solver;

import sudoku.controller.CellController;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * If a possible number of a line accumulate specific block, those number can be removed from the remaining other block
 * cells. It's somehow the opposite of the {@link BlockAlignSolver}.
 */
public class BlockAccumulateSolver implements Solver {
  private final List<CellController> checkCells;
  private final List<CellController> remainingBlock;
  private final List<CellController> remainingLine;

  public BlockAccumulateSolver(List<CellController> checkCells, List<CellController> remainingBlock,
    List<CellController> remainingLine) {
    this.checkCells = checkCells;
    this.remainingBlock = remainingBlock;
    this.remainingLine = remainingLine;
  }

  @Override
  public void solve() {
    Set<Integer> checkPossibleNumbers = new TreeSet<>();
    checkPossibleNumbers.addAll(markAndGetPossibleNumbers(checkCells.get(0)));
    checkPossibleNumbers.addAll(markAndGetPossibleNumbers(checkCells.get(1)));
    checkPossibleNumbers.addAll(markAndGetPossibleNumbers(checkCells.get(2)));

    if (!checkPossibleNumbers.isEmpty()) {
      checkPossibleNumbers.forEach(possibleNumber -> {
        if (remainingLine.stream().noneMatch(c -> c.getPossibleNumbers().contains(possibleNumber))) {
          remainingBlock.forEach(c -> c.removePossibleNumber(possibleNumber));
        }
      });
    }

    try {
      Thread.sleep(30);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
    checkCells.get(0).showRedLayer(false);
    checkCells.get(1).showRedLayer(false);
    checkCells.get(2).showRedLayer(false);
  }

  private Set<Integer> markAndGetPossibleNumbers(CellController cell) {
    if (cell.getPossibleNumbers().size() > 1) {
      cell.showRedLayer(true);
      return cell.getPossibleNumbers();
    }
    return new TreeSet<>();
  }
}
