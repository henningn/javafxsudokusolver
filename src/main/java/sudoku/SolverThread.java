/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package sudoku;

import sudoku.controller.CellController;
import sudoku.controller.PlaygroundController;
import sudoku.solver.BlockAccumulateSolver;
import sudoku.solver.BlockAlignSolver;
import sudoku.solver.GeometrySolver;
import sudoku.solver.SimpleSolver;
import sudoku.solver.SinglePossibleNumberSolver;
import sudoku.solver.Solver;
import sudoku.solver.SwordfishSolver;
import sudoku.solver.TripleCellSolver;
import sudoku.solver.TwinCellSolver;
import sudoku.solver.XWingSolver;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SolverThread extends Thread {

  private final PlaygroundController playground;
  private final List<Solver> solvers = new ArrayList<>();

  public SolverThread(PlaygroundController playground) {
    this.playground = playground;
    init();
  }

  private void init() {
    IntStream.range(0, 9).mapToObj(i -> new SimpleSolver(playground.getBlock(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new SimpleSolver(playground.getRow(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new SimpleSolver(playground.getColumn(i))).forEach(solvers::add);

    for (int i = 0; i < 9; i++) {
      List<CellController> row = playground.getRow(i);
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3 * 3), row));
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3 * 3 + 1), row));
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3 * 3 + 2), row));
    }
    for (int i = 0; i < 9; i++) {
      List<CellController> column = playground.getColumn(i);
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3), column));
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3 + 3), column));
      solvers.add(getBlockAlignSolver(playground.getBlock(i / 3 + 6), column));
    }

    IntStream.range(0, 9).mapToObj(i -> new SinglePossibleNumberSolver(playground.getBlock(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new SinglePossibleNumberSolver(playground.getRow(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new SinglePossibleNumberSolver(playground.getColumn(i))).forEach(solvers::add);

    IntStream.range(0, 9).mapToObj(i -> new TwinCellSolver(playground.getBlock(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new TwinCellSolver(playground.getRow(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new TwinCellSolver(playground.getColumn(i))).forEach(solvers::add);

    IntStream.range(0, 9).mapToObj(i -> new TripleCellSolver(playground.getBlock(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new TripleCellSolver(playground.getRow(i))).forEach(solvers::add);
    IntStream.range(0, 9).mapToObj(i -> new TripleCellSolver(playground.getColumn(i))).forEach(solvers::add);

    for (int i = 0; i < 9; i++) {
      List<CellController> row = playground.getRow(i);
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3 * 3), row));
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3 * 3 + 1), row));
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3 * 3 + 2), row));
    }
    for (int i = 0; i < 9; i++) {
      List<CellController> column = playground.getColumn(i);
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3), column));
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3 + 3), column));
      solvers.add(getBlockAccumulateSolver(playground.getBlock(i / 3 + 6), column));
    }

    solvers.add(new XWingSolver(playground));
    solvers.add(new SwordfishSolver(playground));
    solvers.add(getGeometrySolver("""
      cc.....cc
      cc.....cc
      ..rrrrr..
      ..r...r..
      ..r...r..
      ..r...r..
      ..rrrrr..
      cc.....cc
      cc.....cc
      """));
    solvers.add(getGeometrySolver("""
      r..rrr..r
      .cc...cc.
      .cc...cc.
      r.......r
      r.......r
      r.......r
      .cc...cc.
      .cc...cc.
      r..rrr..r
      """));
  }

  private BlockAlignSolver getBlockAlignSolver(List<CellController> block, List<CellController> line) {
    List<CellController> checkCells = block.stream().filter(line::contains).toList();
    List<CellController> remainingBlock = block.stream().filter(blockCell -> !checkCells.contains(blockCell)).toList();
    List<CellController> remainingLine = line.stream().filter(lineCell -> !checkCells.contains(lineCell)).toList();
    return new BlockAlignSolver(checkCells, remainingBlock, remainingLine);
  }

  private BlockAccumulateSolver getBlockAccumulateSolver(List<CellController> block, List<CellController> line) {
    List<CellController> checkCells = block.stream().filter(line::contains).toList();
    List<CellController> remainingBlock = block.stream().filter(blockCell -> !checkCells.contains(blockCell)).toList();
    List<CellController> remainingLine = line.stream().filter(lineCell -> !checkCells.contains(lineCell)).toList();
    return new BlockAccumulateSolver(checkCells, remainingBlock, remainingLine);
  }

  /**
   * @param field r=ring; c=counterpart
   */
  private GeometrySolver getGeometrySolver(String field) {
    List<CellController> ring = new ArrayList<>();
    List<CellController> counterpart = new ArrayList<>();
    String[] fieldArray = field.split("\n");

    for (int x = 0; x < 9; x++) {
      for (int y = 0; y < 9; y++) {
        String value = String.valueOf(fieldArray[y].charAt(x));
        if (value.equals("r")) {
          ring.add(playground.getCell(x, y));
        } else if (value.equals("c")) {
          counterpart.add(playground.getCell(x, y));
        }
      }
    }

    return new GeometrySolver(ring, counterpart);
  }


  @Override
  public void run() {
    try {
      Thread.sleep(1000);
      while (playground.isValid() && !playground.isEveryNumberRevealed()) {
        List<Integer> possibleNumbersStart = new ArrayList<>(9 * 9 * 9);
        IntStream.range(0, 9).forEach(i -> playground.getBlock(i)
          .forEach(c -> possibleNumbersStart.addAll(c.getPossibleNumbers())));

        solvers.forEach(Solver::solve);

        List<Integer> possibleNumbersEnd = new ArrayList<>(9 * 9 * 9);
        IntStream.range(0, 9).forEach(i -> playground.getBlock(i)
          .forEach(c -> possibleNumbersEnd.addAll(c.getPossibleNumbers())));

        if (possibleNumbersStart.size() == possibleNumbersEnd.size()) {
          break;
        }
      }
      if (!playground.isValid()) {
        System.err.println("Sudoku not valid!");
      } else if (playground.isEveryNumberRevealed()) {
        System.out.println("Sudoku solved.");
      } else {
        System.out.println("Could not solve.");
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
